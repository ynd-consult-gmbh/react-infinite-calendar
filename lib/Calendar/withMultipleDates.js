'use strict';

exports.__esModule = true;
exports.withMultipleDates = exports.enhanceDay = undefined;

var _withState2 = require('recompose/withState');

var _withState3 = _interopRequireDefault(_withState2);

var _withPropsOnChange2 = require('recompose/withPropsOnChange');

var _withPropsOnChange3 = _interopRequireDefault(_withPropsOnChange2);

var _withProps2 = require('recompose/withProps');

var _withProps3 = _interopRequireDefault(_withProps2);

var _compose2 = require('recompose/compose');

var _compose3 = _interopRequireDefault(_compose2);

exports.defaultMultipleDateInterpolation = defaultMultipleDateInterpolation;

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _ = require('./');

var _utils = require('../utils');

var _withMultipleDates = require('../Header/withMultipleDates');

var _withMultipleDates2 = _interopRequireDefault(_withMultipleDates);

var _format = require('date-fns/format');

var _format2 = _interopRequireDefault(_format);

var _parse = require('date-fns/parse');

var _parse2 = _interopRequireDefault(_parse);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

var styles = {
  'root': 'Cal__Day__root',
  'enabled': 'Cal__Day__enabled',
  'highlighted': 'Cal__Day__highlighted',
  'today': 'Cal__Day__today',
  'disabled': 'Cal__Day__disabled',
  'selected': 'Cal__Day__selected',
  'month': 'Cal__Day__month',
  'year': 'Cal__Day__year',
  'selection': 'Cal__Day__selection',
  'day': 'Cal__Day__day',
  'range': 'Cal__Day__range',
  'start': 'Cal__Day__start',
  'end': 'Cal__Day__end',
  'betweenRange': 'Cal__Day__betweenRange'
};
var enhanceDay = exports.enhanceDay = (0, _withPropsOnChange3.default)(['selected'], function (_ref) {
  var _classNames;

  var selected = _ref.selected,
      date = _ref.date;

  var isStart = selected.length ? date === selected[0] : false;
  var isEnd = selected.length ? date === selected[selected.length - 1] : false;
  var isSelected = selected.indexOf(date) !== -1;

  return {
    className: isSelected && (0, _classnames2.default)(styles.range, (_classNames = {}, _classNames[styles.start] = isStart, _classNames[styles.betweenRange] = !isStart && !isEnd, _classNames[styles.end] = isEnd, _classNames)),
    isSelected: isSelected
  };
});

// Enhance year component
var enhanceYears = (0, _withProps3.default)(function (_ref2) {
  var displayDate = _ref2.displayDate;
  return {
    selected: displayDate ? (0, _parse2.default)(displayDate) : null
  };
});

// Enhancer to handle selecting and displaying multiple dates
var withMultipleDates = (0, _compose3.default)(_.withDefaultProps, (0, _withState3.default)('scrollDate', 'setScrollDate', getInitialDate), (0, _withState3.default)('displayDate', 'setDisplayDate', getInitialDate), (0, _utils.withImmutableProps)(function (_ref3) {
  var DayComponent = _ref3.DayComponent,
      HeaderComponent = _ref3.HeaderComponent,
      YearsComponent = _ref3.YearsComponent;
  return {
    DayComponent: enhanceDay(DayComponent),
    HeaderComponent: (0, _withMultipleDates2.default)(HeaderComponent),
    YearsComponent: enhanceYears(YearsComponent)
  };
}), (0, _withProps3.default)(function (_ref4) {
  var displayDate = _ref4.displayDate,
      onSelect = _ref4.onSelect,
      setDisplayDate = _ref4.setDisplayDate,
      scrollToDate = _ref4.scrollToDate,
      props = _objectWithoutProperties(_ref4, ['displayDate', 'onSelect', 'setDisplayDate', 'scrollToDate']);

  return {
    passThrough: {
      Day: {
        onClick: function onClick(date) {
          return handleSelect(date, { onSelect: onSelect, setDisplayDate: setDisplayDate });
        }
      },
      Header: {
        setDisplayDate: setDisplayDate
      },
      Years: {
        displayDate: displayDate,
        onSelect: function onSelect(year, e, callback) {
          return handleYearSelect(year, callback);
        },
        selected: displayDate
      }
    },
    selected: props.selected.filter(function (date) {
      return (0, _utils.sanitizeDate)(date, props);
    }).map(function (date) {
      return (0, _format2.default)(date, 'YYYY-MM-DD');
    })
  };
}));

exports.withMultipleDates = withMultipleDates;
function handleSelect(date, _ref5) {
  var onSelect = _ref5.onSelect,
      setDisplayDate = _ref5.setDisplayDate;

  onSelect(date);
  setDisplayDate(date);
}

function handleYearSelect(date, callback) {
  callback((0, _parse2.default)(date));
}

function getInitialDate(_ref6) {
  var selected = _ref6.selected;

  return selected.length ? selected[0] : new Date();
}

function defaultMultipleDateInterpolation(date, selected) {
  var selectedMap = selected.map(function (date) {
    return (0, _format2.default)(date, 'YYYY-MM-DD');
  });
  var index = selectedMap.indexOf((0, _format2.default)(date, 'YYYY-MM-DD'));

  return index === -1 ? [].concat(selected, [date]) : [].concat(selected.slice(0, index), selected.slice(index + 1));
}