import _withState from 'recompose/withState';
import _withPropsOnChange from 'recompose/withPropsOnChange';
import _withProps from 'recompose/withProps';
import _compose from 'recompose/compose';

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

import classNames from 'classnames';
import { withDefaultProps } from './';
import { sanitizeDate, withImmutableProps } from '../utils';
import enhanceHeader from '../Header/withMultipleDates';
import format from 'date-fns/format';
import parse from 'date-fns/parse';
var styles = {
  'root': 'Cal__Day__root',
  'enabled': 'Cal__Day__enabled',
  'highlighted': 'Cal__Day__highlighted',
  'today': 'Cal__Day__today',
  'disabled': 'Cal__Day__disabled',
  'selected': 'Cal__Day__selected',
  'month': 'Cal__Day__month',
  'year': 'Cal__Day__year',
  'selection': 'Cal__Day__selection',
  'day': 'Cal__Day__day',
  'range': 'Cal__Day__range',
  'start': 'Cal__Day__start',
  'end': 'Cal__Day__end',
  'betweenRange': 'Cal__Day__betweenRange'
};


export var enhanceDay = _withPropsOnChange(['selected'], function (_ref) {
  var _classNames;

  var selected = _ref.selected,
      date = _ref.date;

  var isStart = selected.length ? date === selected[0] : false;
  var isEnd = selected.length ? date === selected[selected.length - 1] : false;
  var isSelected = selected.indexOf(date) !== -1;

  return {
    className: isSelected && classNames(styles.range, (_classNames = {}, _classNames[styles.start] = isStart, _classNames[styles.betweenRange] = !isStart && !isEnd, _classNames[styles.end] = isEnd, _classNames)),
    isSelected: isSelected
  };
});

// Enhance year component
var enhanceYears = _withProps(function (_ref2) {
  var displayDate = _ref2.displayDate;
  return {
    selected: displayDate ? parse(displayDate) : null
  };
});

// Enhancer to handle selecting and displaying multiple dates
var withMultipleDates = _compose(withDefaultProps, _withState('scrollDate', 'setScrollDate', getInitialDate), _withState('displayDate', 'setDisplayDate', getInitialDate), withImmutableProps(function (_ref3) {
  var DayComponent = _ref3.DayComponent,
      HeaderComponent = _ref3.HeaderComponent,
      YearsComponent = _ref3.YearsComponent;
  return {
    DayComponent: enhanceDay(DayComponent),
    HeaderComponent: enhanceHeader(HeaderComponent),
    YearsComponent: enhanceYears(YearsComponent)
  };
}), _withProps(function (_ref4) {
  var displayDate = _ref4.displayDate,
      onSelect = _ref4.onSelect,
      setDisplayDate = _ref4.setDisplayDate,
      scrollToDate = _ref4.scrollToDate,
      props = _objectWithoutProperties(_ref4, ['displayDate', 'onSelect', 'setDisplayDate', 'scrollToDate']);

  return {
    passThrough: {
      Day: {
        onClick: function onClick(date) {
          return handleSelect(date, { onSelect: onSelect, setDisplayDate: setDisplayDate });
        }
      },
      Header: {
        setDisplayDate: setDisplayDate
      },
      Years: {
        displayDate: displayDate,
        onSelect: function onSelect(year, e, callback) {
          return handleYearSelect(year, callback);
        },
        selected: displayDate
      }
    },
    selected: props.selected.filter(function (date) {
      return sanitizeDate(date, props);
    }).map(function (date) {
      return format(date, 'YYYY-MM-DD');
    })
  };
}));

export { withMultipleDates };
function handleSelect(date, _ref5) {
  var onSelect = _ref5.onSelect,
      setDisplayDate = _ref5.setDisplayDate;

  onSelect(date);
  setDisplayDate(date);
}

function handleYearSelect(date, callback) {
  callback(parse(date));
}

function getInitialDate(_ref6) {
  var selected = _ref6.selected;

  return selected.length ? selected[0] : new Date();
}

export function defaultMultipleDateInterpolation(date, selected) {
  var selectedMap = selected.map(function (date) {
    return format(date, 'YYYY-MM-DD');
  });
  var index = selectedMap.indexOf(format(date, 'YYYY-MM-DD'));

  return index === -1 ? [].concat(selected, [date]) : [].concat(selected.slice(0, index), selected.slice(index + 1));
}